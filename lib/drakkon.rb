# General RubyGems
require 'require_all' # Require Helper

# MD5's
require 'digest'

# TTY Helpers
require 'tty-prompt'
require 'tty-spinner'
require 'tty-option'
require 'tty-pager'

# Templates
require 'erb'

# Parsing || Move to vanilla JSON for simplification
# require 'oj'

# Image Handling
require 'mini_magick'

# Semver Parsing
require 'semantic'

# Filewatcher
require 'filewatcher'

# Dev
require 'pry'

# Install handling
require 'zip'

# Audio
require 'wahwah'

# Require this first
require_relative 'drakkon/lib/platform_compat'

# Load All Sub Directories / Order Matters
require_all "#{File.dirname(__FILE__)}/drakkon"

# HashDot Instead of Struct
require 'hash_dot'
Hash.use_dot_syntax = true
Hash.hash_dot_use_default = true
