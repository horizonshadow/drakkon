module Drakkon
  module Skeleton
    # General Helpers for Skeleton Class
    module InstallHelpers
      def do_the_check
        unless File.exist? config_file
          LogBot.fatal('Skeleton', "Drakkon config file not found! #{config_file}")
          exit 0
        end

        # Validate Config Structure
        read_config

        # Valid Structure - Name
        valid_structure?

        # Validate / Find Valid Templates
        load_templates

        # TODO: Some other system for dyanmic reloads?
        # if Hub.config[:skeletons].key?(name)
        #   LogBot.fatal('Skeleton', "Duplicate Skeleton already installed: #{name}")
        #   exit 0
        # end

        # I hate the other syntax
        :return
      end

      def valid_structure?
        unless config.key? :name
          LogBot.fatal('Skeleton', 'Name not found!')
          exit 0
        end

        @name = config[:name].to_sym

        # Validate Versioning
        valid_version?
      end

      def valid_version?
        unless config.key? :version
          LogBot.fatal('Skeleton', 'Version not found')
          exit 0
        end

        Semantic::Version.new config[:version]
      rescue StandardError => e
        LogBot.fatal('Skeleton',
                     "Invalid Version: #{config[:version].pastel.to_s.pastel(:green)}; #{e.message.pastel(:yellow)}")
        exit 0
      end

      def load_templates
        @templates = config[:templates].select do |template|
          valid_template?(template)
        end
      end

      # rubocop:disable Metrics/CyclomaticComplexity
      def valid_template?(template_name)
        # TODO: Implement other sources
        template_config_file = case source
                               when :local
                                 "#{data[:path]}/#{template_name}/skeleton.json"
                               end

        template_config = JSON.parse(File.read(template_config_file), { symbolize_names: true })

        case source
        when :local
          template_config[:path] = "#{data[:path]}/#{template_name}"
        end

        unless template_config.key?(:name)
          LogBot.fatal('Skeleton', "Invalid Template. No name found: #{template_name}")
          return false
        end

        unless template_config.key?(:description)
          LogBot.fatal('Skeleton', "Invalid Template. No description found: #{template_name}")
          return false
        end

        if template_config.key?(:variables) && template_config[:variables].is_a?(Array)
          # Preserve Nest cause line is nasty
          unless template_config[:variables].all? { |x| x.is_a? Hash }
            LogBot.fatal('Skeleton', "Invalid Template Variables: #{template_name}")
            return false
          end

          :do_something_else
        end

        unless template_config.key?(:files)
          LogBot.fatal('Skeleton', "Incomplete Template. Files Not Found: #{template_name}")
          return false
        end

        if template_config[:files].empty?
          LogBot.fatal('Skeleton', "Invalid Template. Files Empty: #{template_name}")
          return false
        end

        unless template_config[:files].all? { |x| x.is_a?(Hash) }
          LogBot.fatal('Skeleton', "Invalid Template Files. Should all be hashes: #{template_name}")
          LogBot.fatal('Skeleton', "Template Details: #{template_config[:files]}")
          return false
        end

        # TODO: Potentially Other Validation
        unless template_config[:files].all? do |file|
          # Root
          full_path = "#{template_config[:path]}/#{file[:source]}"

          if File.exist?(full_path)
            true
          else
            LogBot.fatal('Skeleton', "Invalid Template File #{file}. Source File Missing: #{template_name}")
            false
          end
        end

          LogBot.fatal('Skeleton', "Invalid Template File. Source Files Missing: #{template_name}")
          return false
        end

        # TODO: Individual Validate
        # unless files_exist?(template[:files])
        #   LogBot.warn('Skeleton', "Invalid Template: Not all files found: #{template_name}")
        #   return false
        # end

        true
      end
      # rubocop:enable Metrics/CyclomaticComplexity

      # =============================================================
    end
    # =============================================================
  end
end
