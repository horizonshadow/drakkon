module Drakkon
  module Skeleton
    # General Helpers for Gem Class
    module InstallHelpers
      def prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end

      def read_config
        @config = JSON.parse(File.read(config_file), { symbolize_names: true })
      end

      def config_file
        "#{data[:path]}/drakkon.json"
      end

      # =============================================================
    end
    # =============================================================
  end
end
