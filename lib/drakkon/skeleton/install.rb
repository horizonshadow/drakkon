module Drakkon
  module Skeleton
    # Run Command for CLI
    class Install
      include InstallHelpers
      attr_accessor :args, :source, :data, :config, :name

      def initialize(args = [])
        @args = args
        @data = {}
        source_setup
        do_the_check
        save!
      end

      def save!
        details = {
          **data,
          **config,
          source: source
        }

        Hub.config.skeletons[name] = details
        Hub.write
      end

      #=======================================================
    end
    #=======================================================
  end
end
