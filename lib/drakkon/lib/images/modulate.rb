module Drakkon
  module Images
    # General Image Index Helper
    # https://imagemagick.org/script/command-line-options.php#modulate
    module Modulate
      def self.run!(args = [])
        # Amount
        amount = if args.empty?
                   puts 'Amount Syntax: brightness,saturation,hue'
                   prompt.ask('Amount? (e.g. 100,20): ')
                 else
                   args.shift
                 end

        puts <<~HELP
          Usage
          - Run in the directory you wish to modify the images
          - Modulates images via MiniMagick
            -modulate 100,50,80


          Modulate Amount: #{amount}
            Current Directory;
              #{Dir.pwd.pastel(:yellow)}

          Current Directory;
            #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Modulate!? #{'(Destructive)'.pastel(:red)}"

        start(amount)
      end

      def self.start(amount)
        images.each do |img|
          LogBot.info('Image Mod', img)
          process(img, amount)
        end
      end

      def self.process(file, amount)
        convert = MiniMagick::Tool::Convert.new
        convert << file
        convert.modulate(amount)
        convert << file
        convert.call
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
