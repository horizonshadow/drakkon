module Drakkon
  module Images
    # General Image Index Helper
    module Canvas
      def self.run!(args = [])
        # Sizing
        size = if args.empty?
                 prompt.ask('Size? (e.g. 200x200): ')
               else
                 args.shift
               end

        gravity = if args.empty?
                    prompt.ask('Gravity? (west,east,north,south): ', default: 'south')
                  else
                    args.shift
                  end

        puts <<~HELP
                    Usage [WxH] [gravity]
                    - Run in the directory you wish to modify the images
                    - Adjust images via MiniMagick (extent)
          #{'          '}

          Canvas: Size: #{size}, Gravity: #{gravity}
                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Canvas!? #{'(Destructive)'.pastel(:red)}"

        start(size, gravity)
      end

      def self.start(size, gravity)
        images.each do |img|
          LogBot.info('Image Shift', img)
          process(img, gravity, size)
        end
      end

      def self.process(file, gravity, size)
        image = MiniMagick::Image.open(file)
        LogBot.info('Image Canvas', file)

        img = image.combine_options do |cmd|
          cmd.gravity gravity
          cmd.background 'transparent'
          cmd.extent size
        end

        img.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
