module Drakkon
  module Images
    # General Image Index Helper
    module Rotate
      def self.run!(args = [])
        # Rotate
        angle = if args.empty?
                  prompt.ask('Rotate Angle? (e.g. 90,180,45): ')
                else
                  args.shift
                end

        puts <<~HELP
                    Usage [angle]
                    - Run in the directory you wish to modify the images
                    - Rotate the images via MiniMagick
                    - y / yes / Y / Yes for affirmative args

          Angle: #{angle}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Rotate!? #{'(Destructive)'.pastel(:red)}"

        start(angle)
      end

      def self.start(angle)
        images.each do |img|
          LogBot.info('Image Rotate', img)
          process(img, angle)
        end
      end

      def self.process(file, angle)
        image = MiniMagick::Image.open(file)

        image.rotate(angle)
        image.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
