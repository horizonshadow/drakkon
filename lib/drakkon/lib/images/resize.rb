module Drakkon
  module Images
    # General Image Index Helper
    module Resize
      def self.run!(args = [])
        # Sizing
        size = if args.empty?
                 prompt.ask('Target Size? (e.g. 200x200): ')
               else
                 args.shift
               end

        # Recommend as Options?
        # image.filter 'Sinc'
        # image.filter 'Gaussian'

        # Potentially Support Custom Filters?
        filter = if args.empty?
                   'Lanczos2'
                 else
                   args.shift
                 end

        puts <<~HELP
                    Usage [size] [filter = Lanczos2]
                    - Run in the directory you wish to modify the images
                    - Resizes the images via MiniMagick
                    - Sizes are #{'width'.pastel(:blue)} x #{'height'.pastel(:blue)}
                      - 200x200, 150,175, and etc



          #{'Note: this will be best fit! This will retain aspect ratio!'.pastel(:bright_blue)}

          Filter: #{filter} to Size: #{size}
                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Resize!? #{'(Destructive)'.pastel(:red)}"

        start(filter, size)
      end

      def self.start(filter, size)
        images.each do |img|
          LogBot.info('Image Resize', img)
          process(img, filter, size)
        end
      end

      def self.process(file, filter, size)
        image = MiniMagick::Image.open(file)

        # Ignore if the same for w/h
        return if image.width == size.split('x', 2).first.to_i && image.height == size.split('x', 2).last.to_i

        LogBot.info('Image Resize', "Resizing: #{file}: #{size}")
        image.filter filter
        image.resize size
        image.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
