module Drakkon
  module Images
    # General Image Index Helper
    module List
      def self.run!(args = [])
        puts args
        require "#{Dir.pwd}/app/drakkon/image_index"

        loop do
          r = listy_list(index(args))
          break if r == 'exit'

          puts <<~OUTPUT

            #{r.pastel(:bright_blue)}

            **Drakkon::Images.index['#{r}']


          OUTPUT
        end
      end

      def self.index(args)
        if args.empty?

          Images.index.keys
        else

          idx = args.flat_map do |filter|
            Images.index.keys.grep(/#{filter}/)
          end

          idx.compact.uniq
        end
      end

      def self.listy_list(list)
        prompt.select('Filter:', filter: true) do |menu|
          menu.choice name: 'exit'
          list.each do |img|
            menu.choice name: img
          end
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
