module Drakkon
  module Images
    # General Image Index Helper
    module Index
      def self.index
        @index ||= {}

        @index
      end

      def self.catalog
        # Better way to do this?
        @catalog ||= Hash.new { |hash, key| hash[key] = Hash.new(&hash.default_proc) }

        @catalog
      end

      # No Animations
      def self.sprites
        @sprites ||= {}

        @sprites
      end

      def self.all
        @all ||= []

        @all
      end

      def self.context
        @context ||= Dir.pwd

        @context
      end

      def self.digest
        list = Dir["#{sprites_directory}**/*.png"]
        Digest::MD5.hexdigest(list.map { |x| Digest::MD5.file(x).hexdigest }.join)
      end

      def self.sprites_directory
        "#{context}/sprites/"
      end

      def self.run!(force: false, dir: nil)
        @context = dir || Dir.pwd

        # Make Directory `app/drakkon` if it doesn't exist
        FileUtils.mkdir_p('app/drakkon') unless File.directory?('app/drakkon')

        # Create Directory if sprites directory is missing
        FileUtils.mkdir_p(sprites_directory)

        if Settings.config[:image_digest] == digest && File.exist?("#{context}/app/drakkon/image_index.rb")
          LogBot.info('Images Index', 'Nothing New')
          return unless force
        end

        build_index

        Settings.update(:image_digest, digest)

        File.write("#{context}/app/drakkon/image_index.rb", result)
      end

      def self.build_index
        check sprites_directory
      end

      # rubocop:disable Layout/HeredocIndentation
      def self.result
        <<~RB
          module Drakkon
          	module Images
          		def self.index
          			#{index.inspect}
          		end

          		def self.catalog
          			#{catalog.inspect}
          		end

							def self.catalog_dig(long)
								catalog.dig(*long.split('/'))
							end

							# Return Path from Catalog
          		def self.find_sprite(path)
								catalog.dig(*path.split('/')).values.map(&:path)
          		end

							# Return Path from Catalog
          		def self.find_anim(path)
								catalog.dig(*path.split('/')).keys.map do |key|
									"\#{path}/\#{key}"
								end
          		end

          		def self.sprites
          			#{sprites.inspect}
          		end

          		def self.reset_all
          			#{all.inspect}.each do |sprite|
          				$gtk.reset_sprite "sprites/\#{sprite}.png"
          			end
          		end
          	end
          end
        RB
      end
      # rubocop:enable Layout/HeredocIndentation

      # Recursively Go through
      def self.check(dir = nil)
        LogBot.info('Image Index', "Check: #{dir}")

        # Collect Images
        list = Dir["#{dir}/*"]

        # Ignore Empties
        return if list.empty?

        # Check if its an Animation Directory
        if animation_directory?(list)
          LogBot.info('Image Index', "Animation: #{dir}")
          process_animation(list)
          return
        end

        # Do other things
        Dir["#{dir}/*"].each do |file|
          if File.directory?(file)
            Index.check(file)
          elsif File.extname(file) == '.png'
            process(file)
          end
        end

        :done
      end

      def self.animation_directory?(list)
        # Ignore Directories
        return false if list.any? { |x| File.directory? x }

        list.reject { |x| x.include? 'png' }.empty? && list.all? do |x|
          File.basename(x).split('.png', 2).first.numeric?
        end
      end

      def self.process(file = nil)
        details = dimensions(file)
        details[:path] = file.gsub(sprites_directory, '')[1..].gsub('.png', '')

        # Catalog
        catalog_list = details[:path].split('/')
        catalog_location = []
        catalog_list.each do |idx|
          catalog_location.push idx
          catalog.dig(*catalog_location)
        end
        catalog.dig(*catalog_list).merge! details

        # Full Index Push
        index[details[:path]] = details

        # Just Sprites Push
        sprites[details[:path]] = details

        all.push details[:path]
      end

      def self.dimensions(file)
        img = MiniMagick::Image.open(file)
        # data = "{ path: \"#{root}/#{file.gsub('.png', '')}\", w: #{img.width}, h: #{img.height} }"

        {
          w: img.width,
          h: img.height
        }
      end

      def self.process_animation(list = nil)
        # Append all images to `all`
        list.each do |file|
          # Also store individual image
          process file
          all.push file.gsub(sprites_directory, '')[1..].gsub('.png', '')
        end

        # Nab one to get data
        file = list.first
        details = dimensions(file)

        # Frames and Path
        details[:frames] = list.count
        details[:root_path] = File.dirname(file).gsub(sprites_directory, '')[1..]

        # Store via Root Path
        index[details[:root_path]] = details
      end
      # =========================================================
    end
    # =========================================================
  end
end
