module Drakkon
  module Images
    # General Image Index Helper
    module Biggest
      def self.run!(_args = [])
        start
      end

      def self.start
        LogBot.info('Image Biggest Index')
        index = images.map do |img|
          process(img)
        end

        w = index.max_by(&:w).w
        h = index.max_by(&:h).h

        puts "Largest Size is: { w: #{w}, h: #{h} }"
      end

      def self.process(file)
        # LogBot.info('Image Biggest', file)
        img = MiniMagick::Image.open(file)

        {
          w: img.width,
          h: img.height
        }
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
