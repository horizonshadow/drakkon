module Drakkon
  module Images
    # General Image Index Helper
    module Shift
      # TODO: Deprecate / Fix / Remove?
      # This isn't behaving like I expected it to getting a canvas shift
      def self.run!(args = [])
        # Sizing
        size = if args.empty?
                 prompt.ask('Shift Amount(LRxUD)? (e.g. -10x0): ')
               else
                 args.shift
               end

        gravity = if args.empty?
                    prompt.ask('Gravity? (west,east,north,south): ', default: 'west')
                  else
                    args.shift
                  end

        puts <<~HELP
                    Usage [WxH] [gravity]
                    - Run in the directory you wish to modify the images
                    - Shifts the images via MiniMagick
          #{'          '}

          Shift: Size: #{size}, Gravity: #{gravity}
                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Shift!? #{'(Destructive)'.pastel(:red)}"

        start(size, gravity)
      end

      def self.start(size, gravity)
        images.each do |img|
          LogBot.info('Image Shift', img)
          process(img, gravity, size)
        end
      end

      def self.process(file, gravity, size)
        image = MiniMagick::Image.open(file)
        # image << file
        # convert.colorspace('sRGB')
        # convert.fill('white')
        # convert.colorize(100)
        # convert << file
        # convert.call

        LogBot.info('Image Shift', "Shifting: #{file}")

        img = image.combine_options do |cmd|
          cmd.gravity gravity
          cmd.background 'transparent'
          cmd.splice size
        end

        img.write file

        # image.gravity gravity
        # image.background 'transparent'
        # image.splice size
        # image.call
        # image.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
