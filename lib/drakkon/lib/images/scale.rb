module Drakkon
  module Images
    # General Image Index Helper
    module Scale
      def self.run!(args = [])
        # Sizing
        scale = if args.empty?
                  prompt.ask('Target Scale? (e.g. 100,50,25): ')
                else
                  args.shift
                end

        scale = scale.to_f / 100
        LogBot.info('Image Scale', "Scale: #{scale}")

        # Recommend as Options?
        # image.filter 'Sinc'
        # image.filter 'Gaussian'

        # Potentially Support Custom Filters?
        filter = if args.empty?
                   'Lanczos2'
                 else
                   args.shift
                 end

        puts <<~HELP
                    Usage [size] [filter = Lanczos2]
                    - Run in the directory you wish to modify the images
                    - Resizes the images via MiniMagick
                    - Each image sizing values scaled by (value / 100)

          #{'Note: this will be best fit! This will retain aspect ratio!'.pastel(:bright_blue)}

          Filter: #{filter} to Size: #{scale}
                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Scale Images!? #{'(Destructive)'.pastel(:red)}"
          exit
        end

        start(filter, scale)
      end

      def self.start(filter, scale)
        images.each do |img|
          resize(img, filter, scale)
        end
      end

      def self.resize(file, filter, scale)
        image = MiniMagick::Image.open(file)

        # Calculate Size / Width Primpt
        width = (image.width * scale).round
        height = (image.height * scale).round
        size = "#{width}x#{height}"

        LogBot.info('Image Scale', "#{File.basename file} => #{scale}: #{size}")
        image.filter filter
        image.resize size
        image.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
