module Drakkon
  module Images
    # General Image Index Helper
    module Double
      def self.run!(args = [])
        # Alpha
        alpha = if args.empty?
                  prompt.ask('Amount of Doubled Alpha? (e.g. 0..1): ')
                else
                  args.shift
                end
        LogBot.info('Double Image Alhpa', "Alpha: #{alpha}")

        puts <<~HELP
          Usage [alpha]
                     - Run in the directory you wish to modify the images
                     - Adds the layer onto itself with a alpha value via MiniMagick
          Current Directory;
                       #{Dir.pwd.pastel(:yellow)}

           Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Double Images!? #{'(Destructive)'.pastel(:red)}"
          exit
        end

        start(alpha)
      end

      def self.start(alpha)
        images.each do |img|
          double(img, alpha)
        end
      end

      def self.double(file, alpha)
        image = MiniMagick::Image.open(file)

        LogBot.info('Image Double', "#{File.basename file}")

        # Create a copy of the image for overlay
        overlay = image.clone

        # Apply alpha to the overlay
        overlay = overlay.compose('Over') do |c|
          c.gravity 'center' # Center the overlay
          c.define "compose:args=1,0,0,#{alpha}" # Control the alpha
        end

        # Combine the original image and the overlay
        result = image.composite(overlay) do |c|
          c.gravity 'center' # Center the overlay on the original image
        end

        # Save the result
        result.write(file)
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
