module Drakkon
  module Images
    # General Image Index Helper
    module Desaturate
      def self.run!(_args = [])
        puts <<~HELP
                    Usage [WxH] [gravity]
                    - Run in the directory you wish to modify the images
                    - Adjust images via MiniMagick (extent)

          Desaturate:
                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Desaturate!? #{'(Destructive)'.pastel(:red)}"

        start
      end

      def self.start
        images.each do |img|
          LogBot.info('Image Shift', img)
          process(img)
        end
      end

      def self.process(file)
        image = MiniMagick::Image.open(file)
        LogBot.info('Image Desaturate', file)

        img = image.combine_options do |cmd|
          cmd.colorspace 'Gray'
        end

        img.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
