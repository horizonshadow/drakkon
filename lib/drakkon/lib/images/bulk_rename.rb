module Drakkon
  module Images
    # This renames png's in a directory to match the index_frame 0..10 etc
    module BulkRename
      def self.run!(args = [])
        args ||= []
        sort_method = if args.empty?
                        :mtime
                      else
                        args.shift.to_sym
                      end

        # Lazy Safety
        sort_method = :mtime unless %i[name mtime].include?(sort_method)

        puts <<~HELP
                    Usage [sort_method = mtime, name]
                    - Run in the directory you wish to rename the images (uses mtime)

                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        list = sorted_images(sort_method)

        list.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Rename!? #{'(Destructive)'.pastel(:red)}"

        start(list)
      end

      def self.start(list)
        # Create tmp_dir to send stuff too
        Dir.mktmpdir do |tmp_dir|
          # Move / rename all out into tmp_dir to avoid clobbering
          list.each_with_index do |f, i|
            FileUtils.mv(f, "#{tmp_dir}/#{i}.png")
          end

          # Move all back
          Dir["#{tmp_dir}/*.png"].each do |f|
            FileUtils.mv(f, Dir.pwd)
          end
        end
      end

      def self.sorted_images(sort_method)
        case sort_method
        when :mtime then images.sort_by { |x| File.mtime(x) }
        when :name then images.sort_by { |x| File.basename(x) }
        end
      end

      def self.process(file, filter, size)
        image = MiniMagick::Image.open(file)

        # Ignore if the same for w/h
        return if image.width == size.split('x', 2).first.to_i && image.height == size.split('x', 2).last.to_i

        LogBot.info('Image Resize', "Resizing: #{file}: #{size}")
        image.filter filter
        image.resize size
        image.write file
      end

      def self.images
        # Dir["#{Dir.pwd}/*.png"].sort_by(&:File.basename)

        Dir["#{Dir.pwd}/*.png"].sort_by { |x| File.mtime(x) }
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
