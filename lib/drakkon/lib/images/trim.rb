module Drakkon
  module Images
    # General Image Index Helper
    module Trim
      def self.run!(_args = [])
        puts <<~HELP
                    Usage
                    - Run in the directory you wish to modify the images
                    - Trim/Repage the images via MiniMagick

                  Current Directory;
                    #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Scale Images!? #{'(Destructive)'.pastel(:red)}"
          exit
        end

        start
      end

      def self.start
        images.each do |img|
          process(img)
        end
      end

      def self.process(file)
        LogBot.info('Image Trim', File.basename(file).to_s)
        MiniMagick::Tool::Convert.new do |convert|
          convert << file
          convert.trim
          convert << '+repage'
          convert << file
        end
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
