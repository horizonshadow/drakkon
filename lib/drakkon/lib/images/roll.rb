module Drakkon
  module Images
    # Roll Images like an old TV
    # https://imagemagick.org/Usage/warping/#roll
    module Roll
      def self.run!(args = [])
        # Amount
        amount = if args.empty?
                   prompt.ask('Roll Amount(+-LR+-UD)? (e.g. -10+0): ')
                 else
                   args.shift
                 end

        puts <<~HELP
                    Usage [WxH] [gravity]
                    - Run in the directory you wish to modify the images
                    - Rolls the images via MiniMagick

          Roll: Amount: #{amount}
                    Current Directory;
                      #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Roll!? #{'(Destructive)'.pastel(:red)}"

        start(amount)
      end

      def self.start(amount)
        images.each do |img|
          LogBot.info('Image Roll', img)
          process(img, amount)
        end
      end

      def self.process(file, amount)
        image = MiniMagick::Image.open(file)

        LogBot.info('Image Roll', "Rolling: #{file}")

        img = image.combine_options do |cmd|
          cmd.background 'transparent'
          cmd.roll amount
        end

        img.write file
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
