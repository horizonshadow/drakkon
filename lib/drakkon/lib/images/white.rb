module Drakkon
  module Images
    # General Image Index Helper
    module White
      def self.run!(_args = [])
        puts <<~HELP
          Usage
          - Run in the directory you wish to modify the images
          - Changes the colorspace to 100% white via MiniMagick

          Current Directory;
            #{Dir.pwd.pastel(:yellow)}

          Images:
        HELP

        images.sort.each do |img|
          img_s = img.gsub("#{Dir.pwd}/", '').pastel(:yellow)
          puts "  - #{img_s}"
        end
        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} All White!? #{'(Destructive)'.pastel(:red)}"

        start
      end

      def self.start
        images.each do |img|
          LogBot.info('Image White', img)
          process(img)
        end
      end

      def self.process(file)
        convert = MiniMagick::Tool::Convert.new
        convert << file
        convert.colorspace('sRGB')
        convert.fill('white')
        convert.colorize(100)
        convert << file
        convert.call
      end

      def self.images
        Dir["#{Dir.pwd}/**/*.png"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
