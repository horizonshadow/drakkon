module Drakkon
  module Images
    # General Image Index Helper
    module SplitSpriteSheet
      def self.run!(args = [])
        image = if args.empty?
                  prompt.select('What image to split?', images, filter: true)
                else
                  args.first
                end

        image = "#{Dir.pwd}/#{image}.png"

        unless File.exist?(image)
          LogBot.fatal('Split', "Unable to find: '#{image}'")
          exit 1
        end

        sizing = dimensions(image)

        LogBot.info('Split', "Image Size: #{sizing[:w]}, #{sizing[:h]}")

        # Columns
        columns = if args.empty?
                    prompt.ask('Columns: ', default: 5, convert: :int)
                  else
                    args.shift
                  end

        # Rows
        rows = if args.empty?
                 prompt.ask('Rows: ', default: 5, convert: :int)
               else
                 args.shift
               end

        width = sizing[:w] / columns
        height = sizing[:h] / rows

        puts <<~HELP
                    Usage [file]
                    - Modify one file into a directory of split images


          Split:
              Rows: #{rows}, Columns: #{columns}
              Size: #{width} x #{height}
              Current Directory: #{Dir.pwd.pastel(:yellow)}

          Image to Modify: #{image}
        HELP

        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)} Split!? #{'(Destructive)'.pastel(:red)}"

        process(image, width, height)
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.process(file, width, height)
        LogBot.info('Image Split', file)
        convert = MiniMagick::Tool::Convert.new
        convert << file
        convert.crop("#{width}x#{height}")
        convert << file
        convert.call
      end

      def self.images
        Dir["#{Dir.pwd}/*.png"].map do |x|
          File.basename(x, '.png')
        end.sort
      end

      def self.dimensions(file)
        img = MiniMagick::Image.open(file)

        {
          w: img.width,
          h: img.height
        }
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
