module Drakkon
  module Images
    # General Image Index Helper
    # convert -dispose Background "Background.psd" -layers coalesce "output.png"
    module Layers
      def self.run!(args = [])
        # File Select
        file = if args.empty?
                 prompt.select('File to split?', files, filter: true)
               else
                 args.first
               end

        output = if args.empty?
                   prompt.ask('Output File Pattern? (e.g. output): ')
                 else
                   args.shift
                 end

        puts <<~HELP
               Usage
               - Use image magick to split layers of input file. Specifically intially used for PSD files.

          Input: #{file}
          Output: #{output}
        HELP

        puts

        exit unless prompt.yes? "#{'Are you sure?'.pastel(:bright_yellow)}"

        start(file, output)
      end

      def self.start(file, output)
        MiniMagick::Tool::Convert.new do |convert|
          convert.dispose 'Background'
          convert << file
          convert.layers 'coalesce'
          convert << "#{output}.png"
        end
      end

      def self.files
        Dir["#{Dir.pwd}/*"]
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end
      # =========================================================
    end
    # =========================================================
  end
end
