# ======================
# Debug Helpers
# ======================
# Manifest.list
# Manifest.search('app')
# ======================

module Drakkon
  # Helper to Generate Manifest
  module Manifest
    def self.root_directory
      "#{Dir.pwd}/"
    end

    def self.run!(force: false)
      index

      if Settings.config[:manifest_digest] == digest && File.exist?('app/drakkon/manifest.rb')
        LogBot.info('Manifest', 'Nothing New')
        return unless force
      end

      Settings.update(:manifest_digest, digest)

      LogBot.info('Manifest', 'Saved!')

      # Make Directory `app/drakkon` if it doesn't exist
      FileUtils.mkdir_p('app/drakkon') unless File.directory?('app/drakkon')
      File.write('app/drakkon/manifest.rb', required)
    end

    # Split out | Just collect information
    def self.index
      list
      search("#{Dir.pwd}/app")

      # General Cleanup
      list.reject! { |x| x.include? 'app/main.rb' }

      list.map! do |file|
        file.gsub(root_directory, '')
      end
    end

    def self.digest
      Digest::MD5.hexdigest(list.map { |x| Digest::MD5.file(x).hexdigest }.join)
    end

    def self.list
      @list ||= []

      @list
    end

    def self.search(dir)
      # Skip Drakkon
      return if dir.include?('app/drakkon')

      @list.concat Dir["#{dir}/*.rb"]

      Dir["#{dir}/*"].each do |file|
        next unless File.directory?(file)

        Manifest.search(file)
      end
    end

    # Generate Require Statements
    # Sure there gonna be plenty of opinions on this bad boi
    def self.required
      require_strings = ''
      list.reverse.each do |file|
        require_strings << "require '#{file}'\n"
      end

      require_strings
    end

    # ----------------------
  end
  # ----------------------
end
