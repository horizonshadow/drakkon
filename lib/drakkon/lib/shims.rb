# Make it easier to check for animations
class String
  def numeric?
    !Float(self).nil?
  rescue StandardError
    false
  end
end
