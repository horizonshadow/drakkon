module Drakkon
  module Fonts
    # General Font Index Helper
    module Index
      def self.index
        @index ||= {}

        @index
      end

      def self.context
        @context ||= Dir.pwd

        @context
      end

      def self.digest
        list = Dir["#{fonts_directory}/**/*"].select { |file| File.file?(file) }
        Digest::MD5.hexdigest(list.map { |x| Digest::MD5.file(x).hexdigest }.join)
      end

      def self.fonts_directory
        "#{context}/fonts"
      end

      def self.run!(force: false, dir: nil)
        @context = dir || Dir.pwd

        # Create Directory if sprites directory is missing
        FileUtils.mkdir_p(fonts_directory)

        if Settings.config[:font_digest] == digest && File.exist?("#{context}/app/drakkon/font_index.rb")
          LogBot.info('Fonts Index', 'Nothing New')
          return unless force
        end

        build_index

        Settings.update(:font_digest, digest)

        File.write("#{context}/app/drakkon/font_index.rb", result)
      end

      def self.build_index
        check fonts_directory
      end

      def self.result
        <<~RB
          module Drakkon
            module Fonts
              def self.index
                #{index.inspect}
              end
            end
          end
        RB
      end

      # Recursively Go through
      def self.check(dir = nil)
        LogBot.info('Font Index', "Check: #{dir}")

        # Collect Fonts
        list = Dir["#{dir}/*"]

        # Ignore Empties
        return if list.empty?

        # Do other things
        Dir["#{dir}/*"].each do |file|
          next if File.directory?(file)

          process(file)
        end

        :done
      end

      def self.process(file = nil)
        name = File.basename(file, File.extname(file))
        index[name] = file.gsub(fonts_directory, '')[1..]
      end
      # =========================================================
    end
    # =========================================================
  end
end
