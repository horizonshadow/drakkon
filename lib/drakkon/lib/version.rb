module Drakkon
  # Run Command for CLI
  module Version
    def self.args(raw = nil)
      @args ||= raw

      @args
    end

    def self.list
      LogBot.info('Versions', Hub.versions.join(', '))
    end

    def self.set
      current = "(Current: #{Settings.version.pastel(:bright_blue)})"
      version = prompt.select("Select version #{current}", Hub.versions_sorted, filter: true)

      Settings.update(:version, version)
    end

    # General Run
    def self.install!(raw)
      # Safety if nothing provided
      if raw.nil? || raw.empty?
        LogBot.fatal('Version', 'No Zip Provided!')
        file = prompt.ask('Full path to DragonRuby Zip?')
        unless File.exist?(file)
          LogBot.fatal('Version', 'Not able to find zip!')
          exit(1)
        end
        raw = [file]
      end

      zip = raw.first
      if zip.nil? || File.extname(zip) != '.zip'
        LogBot.fatal('Version', "Invalid Argument! Must be a zip file! '#{zip}'")
        exit(1)
      end

      # rubocop:disable Metrics/BlockLength
      Dir.mktmpdir('drakkon-sauce-') do |tmp|
        # full_path = "#{Dir.pwd}/#{zip}"

        # Unzip install file
        Zip::File.open(zip) do |zip_file|
          zip_file.each do |entry|
            path = File.join(tmp, entry.name)
            FileUtils.mkdir_p(File.dirname(path))

            next if File.exist?(path)

            # Extract
            zip_file.extract(entry, path)

            # Preserve Permissions
            FileUtils.chmod(entry.unix_perms, path)
          end
        end

        # Find Dir
        dr_dir = Dir["#{tmp}/*"].first
        unless dr_dir.include?('dragonruby-')
          LogBot.fatal('Version', "Valid Zip? Not finding DragonRuby Directory, #{dr_dir}")
          exit(2)
        end

        # Find Version
        unless File.exist?("#{dr_dir}/CHANGELOG-CURR.txt")
          LogBot.fatal('Version', "Version File Missing! #{"#{dr_dir}/CHANGELOG-CURR.txt"}")
          exit(2)
        end

        # Collect Version
        dr_version = File.open("#{dr_dir}/CHANGELOG-CURR.txt", &:readline)&.chomp&.split(' ', 2)&.last
        if dr_version.nil?
          LogBot.fatal('Version', "Unable to find DragonRuby Version!, #{dr_version}")
          exit(2)
        end

        # TODO: add overwrite
        if Hub.version?(dr_version)
          LogBot.fatal('Version', "Version already exists!, #{dr_version}")
          exit(3)
        end

        # Copy
        FileUtils.cp_r(dr_dir, "#{Hub.dir}/#{dr_version}/")

        # Save
        Hub.version_add(dr_version)

        # Finish
        LogBot.info('Version', "#{dr_version} Installed!")
      end
      # rubocop:enable Metrics/BlockLength
    end

    def self.prompt
      TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
    end

    #=======================================================
  end
  #=======================================================
end
