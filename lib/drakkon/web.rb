module Drakkon
  # Web Helper
  module Web
    # Free running
    def self.run!(_args = [])
      # Save Current Directory before changing to Version Directory
      @context = Dir.pwd

      # Yes... Run.run!
      Dir.chdir(Run.version_dir) do
        runtime
        output
      end
    rescue SystemExit, Interrupt
      LogBot.info('Web', 'Exiting')
      exit 0
    end

    # Send output to console
    def self.output
      loop do
        next unless @dragonruby.ready?

        puts @dragonruby.readline
      end
    end

    def self.runtime
      @dragonruby = IO.popen(Run.run_env, run_cmd)
    end

    # Exec skips the weird shell stuff
    def self.run_cmd
      ['./dragonruby-httpd', @context, '8000']
    end

    # ==========================================================================
  end
  # ==========================================================================
end
