module Drakkon
  module Gems
    # Placeholders
    module GemHelpers
    end

    # General Encompasing thingy
    class Gem
      include GemHelpers
      attr_accessor :args, :opts, :name, :data, :source, :config, :modules

      def initialize(opts = {})
        @opts = opts
        @data = {}
      end

      def edit(name, details, action = :modules)
        @name = name.to_sym
        @source = details[:source]
        @data = details[:data]
        do_the_check(installing: false)

        case action
        when :modules then select_modules
        when :files then select_files
        when :refresh then refresh_modules
        end

        update_gem_data
      end

      def intall_setup(args)
        @args = args
        source_setup
        do_the_check
        select_modules

        update_gem_data
      end

      def update_gem_data
        gem_data = {
          source: source,
          data: data
        }

        Settings.gems[name.to_sym] = gem_data
        Settings.write
      end

      def prompt_source; end
      # ========================================================================
    end
    # ========================================================================
  end
end
