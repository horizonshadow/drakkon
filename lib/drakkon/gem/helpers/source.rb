module Drakkon
  module Gems
    # General Helpers for Gem Class
    module GemHelpers
      def source_setup
        @source = source?(args.shift).to_sym

        case source
        when :local
          data[:path] = path?(args.shift)
          # Using Name within package
          # @id = "#{source}--#{data[:path]}"
        end
      end

      def source?(source)
        return source if sources.include?(source)

        prompt.select('Gem Source?', sources, filter: true)
      end

      def sources
        [
          'local'
        ]
      end

      def path?(path)
        return path unless path.nil?

        loop do
          dir = prompt.ask('Local Path? (e.g. /data/gem/location)')

          return dir if File.directory?(dir)

          LogBot.fatal('Gem', "Invalid Directory / Not found! #{dir.pastel(:red)}")
        end
      end
      # =============================================================
    end
    # =============================================================
  end
end
