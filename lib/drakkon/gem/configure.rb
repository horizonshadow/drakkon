module Drakkon
  module Gems
    # Run Command for CLI
    module Configure
      def self.opts
        @opts
      end

      def self.start(args)
        if Settings.gems.empty?
          LogBot.fatal('Gem Configure', 'No gems! Install one first!')
          return
        end

        # Allow CLI/Gem Edit
        # TODO: Refactor
        # This feels pretty icky, would probably be good to refactor when my brain is working
        gem_name = args.shift&.to_sym
        gem_name = nil if !gem_name.nil? && !Settings.gems.keys.include?(gem_name)

        gem_name = gem_select if gem_name.nil?
        # ======================================================================

        # Initial Opts
        initial_opt = args.shift&.to_sym

        case initial_opt || modify_options(gem_name)
        when :modules
          gem = Gem.new
          gem.edit(gem_name, Settings.gems[gem_name], :modules)
        when :files
          gem = Gem.new
          gem.edit(gem_name, Settings.gems[gem_name], :files)

        when :delete
          LogBot.info('Gem', "Removing #{gem_name.pastel(:bright_blue)}")
          Settings.gems.delete gem_name
          Settings.write
        when :refresh
          LogBot.info('Gem', "Refreshing  #{gem_name.pastel(:bright_blue)} Modules")
          gem = Gem.new
          gem.edit(gem_name, Settings.gems[gem_name], :refresh)
        end
      rescue TTY::Reader::InputInterrupt
        exit 0
      end

      def self.gem_select
        prompt.select('Which gem do you want to edit?:', filter: true) do |menu|
          Settings.gems.each_key do |gem|
            menu.choice name: gem, value: gem
          end
        end
      end

      def self.modify_options(gem_name)
        prompt.select("Editing #{gem_name.pastel(:bright_blue)}. What do you want to do?", filter: true) do |menu|
          menu.choice name: 'Edit Modules', value: :modules
          menu.choice name: 'Edit Files', value: :files
          menu.choice name: 'Refresh Modules', value: :refresh
          menu.choice name: 'Remove', value: :remove
        end
      end

      def self.prompt
        TTY::Prompt.new(active_color: :cyan, interrupt: :exit)
      end

      #=======================================================
    end
    #=======================================================
  end
end
