# lib = File.expand_path('lib', __dir__)
# $LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

$LOAD_PATH.push File.expand_path('lib', __dir__)
require 'drakkon/release'
Gem::Specification.new do |spec|
  spec.name = 'drakkon'
  spec.version = Drakkon::VERSION
  spec.authors = ['Davin Walker']
  spec.email = ['dishcandanty@gmail.com']

  spec.summary = 'DragonRuby Helpers'
  spec.description = 'DragonRuby Helpers'
  spec.homepage = 'https://gitlab.com/dragon-ruby/drakkon'
  spec.license = 'MIT'

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = ''
    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['documentation_uri'] = 'https://dragon-ruby.gitlab.io/drakkon-docs'
    spec.metadata['source_code_uri'] = 'https://gitlab.com/dragon-ruby/drakkon'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
          'public gem pushes.'
  end

  spec.files = Dir['{bin,lib}/**/*', 'LICENSE', 'README.md']
  spec.executables = %w[drakkon]

  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 3.0'

  spec.add_runtime_dependency 'filewatcher'
  spec.add_runtime_dependency 'hash_dot'
  spec.add_runtime_dependency 'mini_magick'
  spec.add_runtime_dependency 'pry'
  spec.add_runtime_dependency 'require_all', '~> 3.0'
  spec.add_runtime_dependency 'rubyzip'
  spec.add_runtime_dependency 'semantic'
  spec.add_runtime_dependency 'tty-option'
  spec.add_runtime_dependency 'tty-pager'
  spec.add_runtime_dependency 'tty-prompt'
  spec.add_runtime_dependency 'tty-reader'
  spec.add_runtime_dependency 'tty-spinner'
  spec.add_runtime_dependency 'wahwah'

  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
